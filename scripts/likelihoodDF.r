likelihoodDF <- function(file) {
    source("scripts/TruePositiveTrueNegative.r")
    out.df <- data.frame(TruePositiveTrueNegative(file, "1:CYT"),
                         TruePositiveTrueNegative(file, "2:ME3"),
                         TruePositiveTrueNegative(file, "3:MIT"),
                         TruePositiveTrueNegative(file, "4:NUC"),
                         TruePositiveTrueNegative(file))
    
    names(out.df) <- c("CYT", "ME3", "MIT", "NUC", "Average")
    row.names(out.df) <- c("pos", "neg")
    return(round(out.df, 3))
}
