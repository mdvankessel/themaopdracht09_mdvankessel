# Copyright (c) 2018 Maarten van Kessel.
# Licensed under GPLv3. See gpl.md

likelihoodRatio <- function(nTP=0, nFP=0, nTN=0, nFN=0){
    sensitivity <- nTP / (nTP + nFN)
    specificity <- nTN / (nTN + nFP)
    
    LRP <- sensitivity / (1-specificity)
    LRN <- (1-sensitivity) / specificity
    
    return(c(LRP, LRN))
}