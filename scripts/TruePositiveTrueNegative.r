TruePositiveTrueNegative <- function(file, classification = "average") {
    source("scripts/likelihoodRatio.r")
    weka.file <- read.table(file = file, 
                            header = T, 
                            sep = ",")
    
    weka.file <- data.frame(weka.file)
    
    if(classification == "average") {
        # Selects ACTUAL class
        TP.error <- table(weka.file$error[weka.file$actual])
    
        # Selects PREDICTED class
        TN.error <- table(weka.file$error[weka.file$predicted])
        
        # Selects NO ERROR from ACTUAL class
        TP <- TP.error[[1]] / sum(TP.error)
        
        # Selects ERROR from PREDICTED class
        TN <- TN.error[[2]] / sum(TN.error)
        
        # [TP + FN = 1]
        # [FP + TN = 1]
        FN <- 1 - TP
        FP <- 1 - TN
        likelihoods <- likelihoodRatio(nTP = TP, nTN = TN, nFN = FN, nFP = FP)
        return(likelihoods)
        } else {
            # Selects ACTUAL class
            TP.error <- table(weka.file$error[weka.file$actual == classification])

            # Selects PREDICTED class
            TN.error <- table(weka.file$error[weka.file$predicted == classification])
            
            # Selects NO ERROR from ACTUAL class
            TP <- TP.error[[1]] / sum(TP.error)
            
            # Selects ERROR from PREDICTED class
            TN <- TN.error[[2]] / sum(TN.error)
            # [TP + FN = 1]
            # [FP + TN = 1]
            FN <- 1 - TP
            FP <- 1 - TN
            likelihoods <- likelihoodRatio(nTP = TP, nTN = TN, nFN = FN, nFP = FP)
            return(likelihoods)
            }
}

