# Copyright (c) 2018 Maarten van Kessel.
# Licensed under GPLv3. See gpl.md
ggplot.histograms <- function() {
    library(ggplot2)
    source("scripts/multiplot.r")
    yeast.data <- read.table(file = "data/yeast.data", header = FALSE)
    yeast.data.df <- data.frame(yeast.data)
    names(yeast.data.df) <- c("seq.name", "mcg", "gvh", "alm", "mit", "erl", "pox", "vac", "nuc", "class")
    
    mcg <- ggplot(data = yeast.data.df, aes(yeast.data.df$mcg)) + 
        geom_histogram(bins = 20, fill=I("blue"), col=I("white"), alpha=0.5) +
        
        ggtitle("McGeoch's method values of signal \n sequence recognition", "Fig. 3") +
        theme(plot.title=element_text(size=10)) +
        
        xlab("Distribution") +
        #theme(axis.title = element_text(size(5))) +
        
        ylab("Count")
        
    

    gvh <- ggplot(data = yeast.data.df, aes(yeast.data.df$gvh)) + 
        geom_histogram(bins = 20, fill=I("blue"), col=I("white"), alpha=0.5) +
        
        ggtitle("von Heijne's method values of\nsignal sequence recognition", "Fig. 4") +
        theme(plot.title=element_text(size=10)) +
        
        xlab("Distribution") +
        
        ylab("Count")
    
    alm <- ggplot(data = yeast.data.df, aes(yeast.data.df$alm)) + 
        geom_histogram(bins = 20, fill=I("blue"), col=I("white"), alpha=0.5) +
        
        ggtitle("ALOM membrane spanning \n region prediction program", "Fig. 5") +
        theme(plot.title=element_text(size=10)) +
        
        xlab("Distribution") +
        
        ylab("Count")
    
    mit <- ggplot(data = yeast.data.df, aes(yeast.data.df$mit)) + 
        geom_histogram(bins = 20, fill=I("blue"), col=I("white"), alpha=0.5) +
        
        ggtitle("Amino acid content of N-terminal \n (non)mitochondrial proteins", "Fig 6") +
        theme(plot.title=element_text(size=10)) +
        
        xlab("Distribution") +
        
        ylab("Count")
    
    vac <- ggplot(data = yeast.data.df, aes(yeast.data.df$vac)) + 
        geom_histogram(bins = 20, fill=I("blue"), col=I("white"), alpha=0.5) +
        ggtitle("Amino acid content of vacuolar \n and extracellular proteins", "Fig 7") +
        theme(plot.title=element_text(size=10)) +
        
        xlab("Distribution") +
        
        ylab("Count")
    
    nuc <- ggplot(data = yeast.data.df, aes(yeast.data.df$nuc)) + 
        geom_histogram(bins = 20, fill=I("blue"), col=I("white"), alpha=0.5) +
        
        ggtitle("Localization signals of \n (non)nuclear proteins", "Fig 8") +
        theme(plot.title=element_text(size=10)) +
        
        xlab("Distribution") +
        
        ylab("Count")
    
    histograms <- multiplot(mcg, gvh, alm, mit, vac, nuc, cols = 3)
    return(histograms)
}
